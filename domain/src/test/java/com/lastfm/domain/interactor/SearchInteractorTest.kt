package com.lastfm.domain.interactor

import com.lastfm.data.datamodel.models.ImageDto
import com.lastfm.data.datamodel.models.album.search.AlbumSearchDto
import com.lastfm.data.datamodel.models.album.search.AlbumSearchMatchesDto
import com.lastfm.data.datamodel.models.album.search.AlbumSearchResponseDto
import com.lastfm.data.datamodel.models.album.search.AlbumSearchResultsDto
import com.lastfm.data.datamodel.models.artist.search.ArtistSearchDto
import com.lastfm.data.datamodel.models.artist.search.ArtistSearchMatchesDto
import com.lastfm.data.datamodel.models.artist.search.ArtistSearchResponseDto
import com.lastfm.data.datamodel.models.artist.search.ArtistSearchResultsDto
import com.lastfm.data.datamodel.models.track.search.TrackSearchDto
import com.lastfm.data.datamodel.models.track.search.TrackSearchMatchesDto
import com.lastfm.data.datamodel.models.track.search.TrackSearchResponseDto
import com.lastfm.data.datamodel.models.track.search.TrackSearchResultsDto
import com.lastfm.data.datasource.remote.SearchDataSource
import com.lastfm.domain.datamodel.mapper.ImageMapper
import com.lastfm.domain.datamodel.mapper.search.*
import com.lastfm.domain.datamodel.models.search.SearchMatches
import com.lastfm.domain.interactors.SearchInteractor
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * @author Joshua Annang
 * @since 10/10/2017.
 */

class SearchInteractorTest {

    @Mock
    private lateinit var searchDataSource: SearchDataSource

    private lateinit var searchInteractor: SearchInteractor

    private lateinit var trackSearchMatchesMapper: TrackSearchMatchesMapper
    private lateinit var albumSearchMatchesMapper: AlbumSearchMatchesMapper
    private lateinit var artistSearchMatchesMapper: ArtistSearchMatchesMapper


    private val searchMatches: SearchMatches
        get() = SearchMatches(
            trackSearchMatchesMapper.mapToDomain(createTrackSearchMatches()),
            albumSearchMatchesMapper.mapToDomain(createAlbumSearchMatches()),
            artistSearchMatchesMapper.mapToDomain(createArtistSearchMatches())
        )

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        val imageMapper = ImageMapper()

        trackSearchMatchesMapper =
            TrackSearchMatchesMapper(TrackSearchMapper(imageMapper))
        albumSearchMatchesMapper = AlbumSearchMatchesMapper(AlbumSearchMapper(imageMapper))
        artistSearchMatchesMapper = ArtistSearchMatchesMapper(ArtistSearchMapper(imageMapper))

        searchInteractor = SearchInteractor(
            searchDataSource,
            trackSearchMatchesMapper,
            albumSearchMatchesMapper,
            artistSearchMatchesMapper
        )
    }

    @Test
    fun searchGetsResultFromApi() {

        val search = "search"

        `when`(searchDataSource.searchTracks(search))
            .thenReturn(Single.just(createTrackSearchResponse()))

        `when`(searchDataSource.searchAlbums(search))
            .thenReturn(Single.just(createAlbumSearchResponse()))

        `when`(searchDataSource.searchArtists(search))
            .thenReturn(Single.just(createArtistSearchResponse()))

        val testObserver = TestObserver<SearchMatches>()

        searchInteractor.search(search).subscribe(testObserver)

        testObserver.assertValue(searchMatches)

    }

    private fun createTrackSearchResponse(): TrackSearchResponseDto {
        return TrackSearchResponseDto(TrackSearchResultsDto(createTrackSearchMatches()))
    }

    private fun createTrackSearchMatches(): TrackSearchMatchesDto {

        val imageDto = ImageDto("size", "text")

        val trackSearchDto =
            TrackSearchDto("artist", "songname", image = listOf(imageDto))

        return TrackSearchMatchesDto(listOf(trackSearchDto))
    }

    private fun createAlbumSearchResponse(): AlbumSearchResponseDto {

        return AlbumSearchResponseDto(AlbumSearchResultsDto(createAlbumSearchMatches()))
    }

    private fun createAlbumSearchMatches(): AlbumSearchMatchesDto {

        val imageDto = ImageDto("size", "text")

        val albumSearchDto =
            AlbumSearchDto("artist", "albumname", image = listOf(imageDto))

        return AlbumSearchMatchesDto(listOf(albumSearchDto))
    }

    private fun createArtistSearchResponse(): ArtistSearchResponseDto {

        return ArtistSearchResponseDto(ArtistSearchResultsDto(createArtistSearchMatches()))
    }

    private fun createArtistSearchMatches(): ArtistSearchMatchesDto {

        val imageDto = ImageDto("size", "text")

        val artistSearchDto = ArtistSearchDto("artistname", image = listOf(imageDto))

        return ArtistSearchMatchesDto(listOf(artistSearchDto))
    }
}

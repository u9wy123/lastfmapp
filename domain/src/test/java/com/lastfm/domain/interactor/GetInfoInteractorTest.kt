package com.lastfm.domain.interactor

import com.lastfm.domain.datamodel.mapper.ImageMapper
import com.lastfm.data.datamodel.models.ImageDto
import com.lastfm.data.datamodel.models.album.info.album.AlbumInfoDto
import com.lastfm.data.datamodel.models.album.info.album.AlbumInfoResponseDto
import com.lastfm.data.datamodel.models.album.info.artist.ArtistInfoDto
import com.lastfm.data.datamodel.models.album.info.artist.ArtistInfoResponseDto
import com.lastfm.data.datamodel.models.album.info.artist.BioInfoDto
import com.lastfm.data.datamodel.models.album.info.track.TrackInfoDto
import com.lastfm.data.datamodel.models.album.info.track.TrackInfoResponseDto
import com.lastfm.data.datamodel.models.album.info.track.TracksInfoDto
import com.lastfm.data.datasource.remote.GetInfoDataSource
import com.lastfm.domain.datamodel.mapper.info.*
import com.lastfm.domain.datamodel.models.info.album.AlbumInfo
import com.lastfm.domain.datamodel.models.info.artist.ArtistInfo
import com.lastfm.domain.datamodel.models.info.track.TrackInfo
import com.lastfm.domain.interactors.GetInfoInteractor
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class GetInfoInteractorTest {

    @Mock
    private lateinit var infoDataSource: GetInfoDataSource

    private lateinit var getInfoInteractor: GetInfoInteractor

    private lateinit var artistInfoMapper: ArtistInfoMapper
    private lateinit var albumInfoMapper: AlbumInfoMapper
    private lateinit var trackInfoMapper: TrackInfoMapper

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        val imageMapper = ImageMapper()

        artistInfoMapper = ArtistInfoMapper(
            BioInfoMapper(),
            imageMapper
        )
        trackInfoMapper = TrackInfoMapper(artistInfoMapper)
        albumInfoMapper = AlbumInfoMapper(imageMapper, TracksInfoMapper(trackInfoMapper))

        getInfoInteractor = GetInfoInteractor(infoDataSource, artistInfoMapper, albumInfoMapper, trackInfoMapper)

    }

    @Test
    fun getArtistInfoReturnsResultFromApi() {

        val artist = "artist"

        `when`(infoDataSource.getArtistInfo(artist))
            .thenReturn(Single.just(createArtistInfoResponseDto()))

        val testObserver = TestObserver<ArtistInfo>()

        getInfoInteractor.getArtistInfo(artist).subscribe(testObserver)

        testObserver.assertValue(createArtistInfo())
    }

    @Test
    fun getAlbumInfoReturnsResultFromApi() {

        val album = "album"
        val artist = "artist"

        `when`(infoDataSource.getAlbumInfo(album, artist))
            .thenReturn(Single.just(createAlbumInfoResponseDto()))

        val testObserver = TestObserver<AlbumInfo>()

        getInfoInteractor.getAlbumInfo(album, artist).subscribe(testObserver)

        testObserver.assertValue(createAlbumInfo())
    }

    @Test
    fun getTrackInfoReturnsResultFromApi() {

        val track = "track"
        val artist = "artist"

        `when`(infoDataSource.getTrackInfo(track, artist))
            .thenReturn(Single.just(createTrackInfoResponseDto()))

        val testObserver = TestObserver<TrackInfo>()

        getInfoInteractor.getTrackInfo(track, artist).subscribe(testObserver)

        testObserver.assertValue(createTrackInfo())
    }

    private fun createArtistInfo(): ArtistInfo {
        return artistInfoMapper.mapToDomain(
            createArtistInfoResponseDto()
                .artistInfoDto
        )
    }

    private fun createArtistInfoResponseDto(): ArtistInfoResponseDto {

        return ArtistInfoResponseDto(createArtistInfoDto())
    }

    private fun createAlbumInfo(): AlbumInfo {
        return albumInfoMapper.mapToDomain(
            createAlbumInfoResponseDto()
                .albumInfoDto
        )
    }

    private fun createArtistInfoDto(): ArtistInfoDto {
        val bioInfoDto = BioInfoDto("summary")

        val imageDto = ImageDto("size", "text")

        return ArtistInfoDto("artistName", listOf(imageDto), bioInfoDto)
    }

    private fun createAlbumInfoResponseDto(): AlbumInfoResponseDto {

        val imageDto = ImageDto("size", "text")

        val tracksInfoDto = TracksInfoDto(listOf(createTrackInfoDto()))

        val albumInfoDto = AlbumInfoDto(
            "albumName",
            "artist",
            "mbid",
            listOf(imageDto),
            tracksInfoDto
        )

        return AlbumInfoResponseDto(albumInfoDto)
    }

    private fun createTrackInfo(): TrackInfo {
        return trackInfoMapper.mapToDomain(createTrackInfoDto())
    }

    private fun createTrackInfoDto(): TrackInfoDto {
        return TrackInfoDto(
            "trackName", "1234", createArtistInfoDto()
        )
    }

    private fun createTrackInfoResponseDto(): TrackInfoResponseDto {

        return TrackInfoResponseDto(createTrackInfoDto())
    }
}

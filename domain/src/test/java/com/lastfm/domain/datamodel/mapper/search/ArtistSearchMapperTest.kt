package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.data.datamodel.models.artist.search.ArtistSearchDto
import com.lastfm.domain.datamodel.mapper.ImageMapper

import org.junit.Before
import org.junit.Test

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat

class ArtistSearchMapperTest {

    private lateinit var artistSearchMapper: ArtistSearchMapper

    @Before
    fun setUp() {
        artistSearchMapper = ArtistSearchMapper(ImageMapper())
    }

    @Test
    fun artistMapsToDomain() {

        val artistSearchDto = createArtistDto()

        val (name) = artistSearchMapper.mapToDomain(artistSearchDto)

        assertThat(name, `is`(artistSearchDto.name))
    }


    private fun createArtistDto(): ArtistSearchDto {
        return ArtistSearchDto("artistName")
    }
}

package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.data.datamodel.models.artist.search.ArtistSearchDto
import com.lastfm.data.datamodel.models.artist.search.ArtistSearchMatchesDto
import com.lastfm.domain.datamodel.mapper.ImageMapper

import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Test

import org.hamcrest.CoreMatchers.`is`

class ArtistSearchMatchesMapperTest {

    private var artistSearchMatchesMapper: ArtistSearchMatchesMapper? = null

    @Before
    fun setUp() {
        artistSearchMatchesMapper =
            ArtistSearchMatchesMapper(ArtistSearchMapper(ImageMapper()))
    }

    @Test
    fun artistSearchMatchesMapsToDomain() {

        val artistSearchMatchesDto = createArtistSearchMatchesDto()

        val (artistSearches) = artistSearchMatchesMapper!!.mapToDomain(artistSearchMatchesDto)

        MatcherAssert.assertThat(
            artistSearches[0].name, `is`(artistSearchMatchesDto.artists?.get(0)?.name)
        )

    }

    private fun createArtistSearchMatchesDto(): ArtistSearchMatchesDto {
        return ArtistSearchMatchesDto(listOf(ArtistSearchDto("artistName")))
    }
}

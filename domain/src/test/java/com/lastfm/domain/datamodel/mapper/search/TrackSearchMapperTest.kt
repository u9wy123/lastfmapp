package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.data.datamodel.models.track.search.TrackSearchDto
import com.lastfm.domain.datamodel.mapper.ImageMapper

import org.junit.Before
import org.junit.Test

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat

class TrackSearchMapperTest {

    private var trackSearchMapper: TrackSearchMapper? = null

    @Before
    fun SetUp() {
        trackSearchMapper = TrackSearchMapper(ImageMapper())
    }

    @Test
    fun TrackMapsToDomain() {

        val trackSearchDto = createTrackDto()

        val subject = trackSearchMapper!!.mapToDomain(trackSearchDto)

        assertThat(subject.name, `is`(trackSearchDto.name))
        assertThat(subject.artist, `is`(trackSearchDto.artist))
    }

    private fun createTrackDto(): TrackSearchDto {
        return TrackSearchDto("trackName", "artistName")
    }
}

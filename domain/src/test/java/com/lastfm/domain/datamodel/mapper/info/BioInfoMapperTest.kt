package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.data.datamodel.models.album.info.artist.BioInfoDto
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class BioInfoMapperTest {

    private lateinit var bioInfoMapper: BioInfoMapper

    @Before
    fun setUp() {
        bioInfoMapper = BioInfoMapper()
    }

    @Test
    fun bioInfoMapsToDomain() {

        val bioInfoDto = createBioInfoDto()

        val (summary) = bioInfoMapper.mapToDomain(bioInfoDto)

        assertThat(summary, `is`(bioInfoDto.summary))
    }

    private fun createBioInfoDto(): BioInfoDto {

        return BioInfoDto("summary")
    }

}

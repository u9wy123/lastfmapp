package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.data.datamodel.models.album.info.track.TrackInfoDto
import com.lastfm.data.datamodel.models.album.info.track.TracksInfoDto
import com.lastfm.domain.datamodel.mapper.ImageMapper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class TracksInfoMapperTest {

    private lateinit var trackInfoMapper: TrackInfoMapper
    private lateinit var tracksInfoMapper: TracksInfoMapper

    @Before
    fun setUp() {
        trackInfoMapper = TrackInfoMapper(
            ArtistInfoMapper(BioInfoMapper(), ImageMapper()))
        tracksInfoMapper = TracksInfoMapper(trackInfoMapper)
    }

    @Test
    fun tracksInfoMapsToDomain() {

        val tracksInfoDto = createTracksInfoDto()

        val (trackInfo) = tracksInfoMapper.mapToDomain(tracksInfoDto)

        assertThat(trackInfo?.get(0)?.name, `is`(tracksInfoDto.trackInfoDto?.get(0)?.name))
        assertThat(trackInfo?.get(0)?.duration, `is`(tracksInfoDto.trackInfoDto?.get(0)?.duration))
    }

    private fun createTracksInfoDto(): TracksInfoDto {

        return TracksInfoDto(listOf(TrackInfoDto("name","duration")))
    }


}

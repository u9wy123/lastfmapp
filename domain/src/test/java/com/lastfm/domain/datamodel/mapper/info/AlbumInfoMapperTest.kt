package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.data.datamodel.models.ImageDto
import com.lastfm.data.datamodel.models.album.info.album.AlbumInfoDto
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class AlbumInfoMapperTest {

    private lateinit var albumInfoMapper: AlbumInfoMapper

    @Before
    fun setUp() {
        val imageMapper = com.lastfm.domain.datamodel.mapper.ImageMapper()
        albumInfoMapper = AlbumInfoMapper(
            imageMapper,
            TracksInfoMapper(TrackInfoMapper(ArtistInfoMapper(BioInfoMapper(), imageMapper))))
    }

    @Test
    fun albumInfoMapsToDomain() {

        val albumInfoDto = createAlbumInfoDto()

        val subject = albumInfoMapper.mapToDomain(albumInfoDto)

        assertThat(subject.name, `is`(albumInfoDto.name))
        assertThat(subject.artist, `is`(albumInfoDto.artist))
    }

    private fun createAlbumInfoDto(): AlbumInfoDto {

        return AlbumInfoDto("name", "artist", "mbid", listOf(ImageDto()))
    }
}

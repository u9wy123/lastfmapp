package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.data.datamodel.models.album.search.AlbumSearchDto
import com.lastfm.data.datamodel.models.album.search.AlbumSearchMatchesDto
import com.lastfm.domain.datamodel.mapper.ImageMapper

import org.junit.Before
import org.junit.Test

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.*

class AlbumSearchMatchesMapperTest {

    private lateinit var albumSearchMatchesMapper: AlbumSearchMatchesMapper

    @Before
    fun setUp() {
        albumSearchMatchesMapper =
            AlbumSearchMatchesMapper(AlbumSearchMapper(ImageMapper()))
    }

    @Test
    fun albumSearchMatchesMapsToDomain() {

        val albumSearchMatchesDto = createAlbumSearchMatchesDto()

        val (albumSearches) = albumSearchMatchesMapper.mapToDomain(albumSearchMatchesDto)

        assertThat(albumSearches[0].name, `is`(albumSearchMatchesDto.albums?.get(0)?.name))
        assertThat(albumSearches[0].artist, `is`(albumSearchMatchesDto.albums?.get(0)?.artist))

    }

    private fun createAlbumSearchMatchesDto(): AlbumSearchMatchesDto {
        return AlbumSearchMatchesDto(listOf(AlbumSearchDto("albumName","artist")))
    }

}

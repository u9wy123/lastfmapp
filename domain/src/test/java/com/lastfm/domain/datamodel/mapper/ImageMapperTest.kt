package com.lastfm.domain.datamodel.mapper

import com.lastfm.data.datamodel.models.ImageDto

import org.junit.Before
import org.junit.Test

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat

class ImageMapperTest {

    private lateinit var imageMapper: ImageMapper

    @Before
    fun setUp() { imageMapper = ImageMapper()
    }

    @Test
    fun artistMapsToDomain() {

        val imageDto = createImageDto()

        val subject = imageMapper.mapToDomain(imageDto)

        assertThat(subject.text, `is`(imageDto.text))
        assertThat(subject.size, `is`(imageDto.size))
    }


    private fun createImageDto(): ImageDto {
       return ImageDto("text","size")
    }
}

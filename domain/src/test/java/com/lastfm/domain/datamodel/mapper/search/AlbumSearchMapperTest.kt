package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.data.datamodel.models.album.search.AlbumSearchDto
import com.lastfm.domain.datamodel.mapper.ImageMapper

import org.junit.Before
import org.junit.Test

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat

class AlbumSearchMapperTest {

    private lateinit var albumSearchMapper: AlbumSearchMapper

    @Before
    fun setUp() {
        albumSearchMapper = AlbumSearchMapper(ImageMapper())
    }

    @Test
    fun albumMapsToDomain() {

        val albumSearchDto = createAlbumDto()

        val subject = albumSearchMapper.mapToDomain(albumSearchDto)

        assertThat(subject.name, `is`(albumSearchDto.name))
        assertThat(subject.artist, `is`(albumSearchDto.artist))
    }

    private fun createAlbumDto(): AlbumSearchDto {
        return AlbumSearchDto("name","artist")
    }

}

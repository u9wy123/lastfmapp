package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.data.datamodel.models.album.info.track.TrackInfoDto
import com.lastfm.domain.datamodel.mapper.ImageMapper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class TrackInfoMapperTest {

    private lateinit var trackInfoMapper: TrackInfoMapper

    @Before
    fun setUp() {
        trackInfoMapper = TrackInfoMapper(
            ArtistInfoMapper(BioInfoMapper(), ImageMapper()))
    }

    @Test
    fun trackInfoMapsToDomain() {

        val trackInfoDto = createTrackInfoDto()

        val (name) = trackInfoMapper.mapToDomain(trackInfoDto)

        assertThat(name, `is`(trackInfoDto.name))
    }

    private fun createTrackInfoDto(): TrackInfoDto {

        return TrackInfoDto("name","duration")
    }
}

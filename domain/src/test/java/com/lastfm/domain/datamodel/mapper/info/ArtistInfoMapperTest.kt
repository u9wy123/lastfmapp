package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.data.datamodel.models.ImageDto
import com.lastfm.data.datamodel.models.album.info.artist.ArtistInfoDto
import com.lastfm.data.datamodel.models.album.info.artist.BioInfoDto
import com.lastfm.domain.datamodel.mapper.ImageMapper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class ArtistInfoMapperTest {

    private lateinit var artistInfoMapper: ArtistInfoMapper

    @Before
    fun setUp() {
        artistInfoMapper = ArtistInfoMapper(
            BioInfoMapper(), ImageMapper())
    }

    @Test
    fun artistInfoMapsToDomain() {

        val artistInfoDto = createArtistInfoDto()

        val (name) = artistInfoMapper.mapToDomain(artistInfoDto)

        assertThat(name, `is`(artistInfoDto.name))
    }

    private fun createArtistInfoDto(): ArtistInfoDto {

        return ArtistInfoDto("name",listOf(ImageDto()),BioInfoDto())
    }

}

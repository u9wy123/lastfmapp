package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.data.datamodel.models.track.search.TrackSearchDto
import com.lastfm.data.datamodel.models.track.search.TrackSearchMatchesDto
import com.lastfm.domain.datamodel.mapper.ImageMapper

import org.junit.Before
import org.junit.Test

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.*

class TrackSearchMatchesMapperTest {

    private lateinit var trackSearchMatchesMapper: TrackSearchMatchesMapper

    @Before
    fun setUp() {
        trackSearchMatchesMapper =
            TrackSearchMatchesMapper(TrackSearchMapper(ImageMapper()))
    }

    @Test
    fun trackSearchMatchesMapsToDomain() {

        val trackSearchMatchesDto = createTrackSearchMatchesDto()

        val (trackSearches) = trackSearchMatchesMapper.mapToDomain(trackSearchMatchesDto)

        assertThat(
            trackSearches[0].name, `is`(trackSearchMatchesDto.tracks?.get(0)?.name)
        )

        assertThat(
            trackSearches[0].artist, `is`(trackSearchMatchesDto.tracks?.get(0)?.artist)
        )

    }

    private fun createTrackSearchMatchesDto(): TrackSearchMatchesDto {
        return TrackSearchMatchesDto(listOf(TrackSearchDto("trackName", "artistName")))
    }

}

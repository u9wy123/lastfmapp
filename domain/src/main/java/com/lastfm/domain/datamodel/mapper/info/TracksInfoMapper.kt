package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.data.datamodel.models.album.info.track.TracksInfoDto
import com.lastfm.domain.datamodel.models.info.track.TrackInfo
import com.lastfm.domain.datamodel.models.info.track.TracksInfo
import java.util.*
import javax.inject.Inject

class TracksInfoMapper @Inject
constructor(protected val trackInfoMapper: TrackInfoMapper) {

    fun mapToDomain(data: TracksInfoDto?): TracksInfo {

        if (data?.trackInfoDto == null) {
            return TracksInfo(emptyList())
        }

        val trackInfoList = ArrayList<TrackInfo>()

        for (trackInfo in data.trackInfoDto!!) {
            trackInfoList.add(trackInfoMapper.mapToDomain(trackInfo))
        }

        return TracksInfo(trackInfoList)
    }
}

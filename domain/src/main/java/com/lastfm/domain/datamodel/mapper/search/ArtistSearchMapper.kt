package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.domain.datamodel.mapper.ImageMapper
import com.lastfm.data.datamodel.models.artist.search.ArtistSearchDto
import com.lastfm.domain.datamodel.Image
import com.lastfm.domain.datamodel.models.search.artist.ArtistSearch
import javax.inject.Inject

class ArtistSearchMapper @Inject
constructor(val imageMapper: ImageMapper) {

    fun mapToDomain(data: ArtistSearchDto): ArtistSearch {

        val image =
            if (data.image == null) Image(
                null,
                null
            ) else imageMapper.mapToDomain(data.image!![data.image!!.size - 1])

        return ArtistSearch(
            data.name, image
        )
    }
}

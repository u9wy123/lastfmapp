package com.lastfm.domain.datamodel.models.search

import com.lastfm.domain.datamodel.models.search.album.AlbumSearchMatches
import com.lastfm.domain.datamodel.models.search.artist.ArtistSearchMatches
import com.lastfm.domain.datamodel.models.search.track.TrackSearchMatches

data class SearchMatches(
    val trackSearchMatches: TrackSearchMatches,
    val albumSearchMatches: AlbumSearchMatches,
    val artistSearchMatches: ArtistSearchMatches
) {

    override fun equals(obj: Any?): Boolean {

        if (obj == null || obj !is SearchMatches) {
            return false
        }

        if (trackSearchMatches.trackSearches.size == obj.trackSearchMatches.trackSearches.size) {

            val compareMatches = obj.trackSearchMatches

            for (i in 0 until trackSearchMatches.trackSearches.size) {
                val name = trackSearchMatches.trackSearches[i].name
                val artist = trackSearchMatches.trackSearches[i].artist
                val image = trackSearchMatches.trackSearches[i].image

                val nameCompare = compareMatches.trackSearches[i].name
                val artistCompare = compareMatches.trackSearches[i].artist
                val imageCompare = compareMatches.trackSearches[i].image

                if (name != nameCompare ||
                    artist != artistCompare ||
                    image != imageCompare
                ) {
                    return false
                }
            }
        }

        if (albumSearchMatches.albumSearches.size == obj.albumSearchMatches.albumSearches.size) {

            val compareMatches = obj.albumSearchMatches

            for (i in 0 until albumSearchMatches.albumSearches.size) {
                val name = albumSearchMatches.albumSearches[i].name
                val artist = albumSearchMatches.albumSearches[i].artist
                val image = albumSearchMatches.albumSearches[i].image

                val nameCompare = compareMatches.albumSearches[i].name
                val artistCompare = compareMatches.albumSearches[i].artist
                val imageCompare = compareMatches.albumSearches[i].image

                if (name != nameCompare ||
                    artist != artistCompare ||
                    image != imageCompare
                ) {
                    return false
                }
            }
        }

        if (artistSearchMatches.artistSearches.size == obj.artistSearchMatches.artistSearches.size) {

            val compareMatches = obj.artistSearchMatches

            for (i in 0 until artistSearchMatches.artistSearches.size) {
                val name = artistSearchMatches.artistSearches[i].name
                val image = artistSearchMatches.artistSearches[i].image

                val nameCompare = compareMatches.artistSearches[i].name
                val imageCompare = compareMatches.artistSearches[i].image

                if (name != nameCompare || image != imageCompare) {
                    return false
                }
            }
        }

        return true
    }

    override fun hashCode(): Int {
        var result = trackSearchMatches.hashCode()
        result = 31 * result + albumSearchMatches.hashCode()
        result = 31 * result + artistSearchMatches.hashCode()
        return result
    }
}

package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.data.datamodel.models.artist.search.ArtistSearchMatchesDto
import com.lastfm.domain.datamodel.models.search.artist.ArtistSearch
import com.lastfm.domain.datamodel.models.search.artist.ArtistSearchMatches
import java.util.*
import javax.inject.Inject

class ArtistSearchMatchesMapper @Inject constructor(
    protected val artistSearchMapper: ArtistSearchMapper) {

    fun mapToDomain(data: ArtistSearchMatchesDto?): ArtistSearchMatches {

        if (data?.artists == null) {
            return ArtistSearchMatches(emptyList())
        }

        val artistSearchList = ArrayList<ArtistSearch>()

        for (artistSearchDto in data.artists!!) {
            artistSearchList.add(artistSearchMapper.mapToDomain(artistSearchDto))
        }

        return ArtistSearchMatches(artistSearchList)
    }
}

package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.domain.datamodel.mapper.ImageMapper
import com.lastfm.data.datamodel.models.track.search.TrackSearchDto
import com.lastfm.domain.datamodel.Image
import com.lastfm.domain.datamodel.models.search.track.TrackSearch
import javax.inject.Inject

class TrackSearchMapper @Inject constructor(val imageMapper: ImageMapper) {

    fun mapToDomain(data: TrackSearchDto): TrackSearch {

        val image = if (data.image == null) Image(
            null,
            null
        ) else imageMapper.mapToDomain(data.image!![data.image!!.size - 1])


        return TrackSearch(data.name, data.artist, image)
    }
}

package com.lastfm.domain.datamodel.models.search.artist

data class ArtistSearchMatches(
    val artistSearches: List<ArtistSearch>
) {

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }
        val other = obj as ArtistSearchMatches

        if (other.artistSearches.size != artistSearches.size) {
            return false
        }

        for (i in artistSearches.indices) {
            val otherArtistSearch = other.artistSearches[i]
            val artistSearch = artistSearches[i]

            if (artistSearch != otherArtistSearch) {
                return false
            }
        }

        return true
    }

    override fun hashCode(): Int {
        return artistSearches.hashCode()
    }
}

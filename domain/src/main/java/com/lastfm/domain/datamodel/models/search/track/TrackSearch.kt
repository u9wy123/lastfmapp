package com.lastfm.domain.datamodel.models.search.track

import com.lastfm.domain.datamodel.Image



data class TrackSearch(
    val name: String? = null,
    val artist: String? = null,
    val image: Image
) {

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }
        val other = obj as TrackSearch
        return (this.name == other.name
                && this.artist == other.artist
                && this.image == other.image)
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + (artist.hashCode())
        result = 31 * result + (image.hashCode())
        return result
    }
}

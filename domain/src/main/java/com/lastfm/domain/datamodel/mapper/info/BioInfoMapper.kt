package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.data.datamodel.models.album.info.artist.BioInfoDto
import com.lastfm.domain.datamodel.models.info.artist.BioInfo

import javax.inject.Inject

class BioInfoMapper @Inject constructor() {

    fun mapToDomain(data: BioInfoDto?): BioInfo {

        return if (data == null) {
            BioInfo(null)
        } else BioInfo(data.summary)
    }
}

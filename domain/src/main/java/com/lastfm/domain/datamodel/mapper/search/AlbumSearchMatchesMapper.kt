package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.data.datamodel.models.album.search.AlbumSearchMatchesDto
import com.lastfm.domain.datamodel.models.search.album.AlbumSearch
import com.lastfm.domain.datamodel.models.search.album.AlbumSearchMatches
import java.util.*
import javax.inject.Inject

class AlbumSearchMatchesMapper @Inject constructor(
    protected val albumSearchMapper: AlbumSearchMapper) {

    fun mapToDomain(data: AlbumSearchMatchesDto?): AlbumSearchMatches {

        if (data?.albums == null) {
            return AlbumSearchMatches(emptyList())
        }

        val albumSearchList = ArrayList<AlbumSearch>()

        for (albumSearchDto in data.albums!!) {
            albumSearchList.add(albumSearchMapper.mapToDomain(albumSearchDto))
        }

        return AlbumSearchMatches(albumSearchList)
    }
}

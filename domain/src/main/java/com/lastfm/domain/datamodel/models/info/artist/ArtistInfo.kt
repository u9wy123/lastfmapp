package com.lastfm.domain.datamodel.models.info.artist


import com.lastfm.domain.datamodel.Image



data class ArtistInfo(
    val name: String? = null,
    val image: Image? = null,
    val bioInfo: BioInfo? = null
) {

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }
        val other = obj as ArtistInfo
        return (this.name == other.name
                && this.image == other.image
                && this.bioInfo == other.bioInfo)
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + (image?.hashCode() ?: 0)
        result = 31 * result + (bioInfo?.hashCode() ?: 0)
        return result
    }
}

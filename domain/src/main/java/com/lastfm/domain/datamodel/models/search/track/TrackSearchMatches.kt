package com.lastfm.domain.datamodel.models.search.track

data class TrackSearchMatches(val trackSearches: List<TrackSearch>) {


    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }
        val other = obj as TrackSearchMatches

        if (other.trackSearches.size != trackSearches.size) {
            return false
        }

        for (i in trackSearches.indices) {
            val otherTrackSearch = other.trackSearches[i]
            val trackSearch = trackSearches[i]

            if (trackSearch != otherTrackSearch) {
                return false
            }
        }

        return true
    }

    override fun hashCode(): Int {
        return trackSearches.hashCode()
    }
}

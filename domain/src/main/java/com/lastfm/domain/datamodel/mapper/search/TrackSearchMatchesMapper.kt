package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.data.datamodel.models.track.search.TrackSearchMatchesDto
import com.lastfm.domain.datamodel.models.search.track.TrackSearch
import com.lastfm.domain.datamodel.models.search.track.TrackSearchMatches
import java.util.*
import javax.inject.Inject

class TrackSearchMatchesMapper @Inject constructor(
    protected val trackSearchMapper: TrackSearchMapper) {

    fun mapToDomain(data: TrackSearchMatchesDto?): TrackSearchMatches {

        if (data?.tracks == null) {
            return TrackSearchMatches(emptyList())
        }

        val trackSearchList = ArrayList<TrackSearch>()

        for (trackSearchDto in data.tracks!!) {
            trackSearchList.add(trackSearchMapper.mapToDomain(trackSearchDto))
        }

        return TrackSearchMatches(trackSearchList)
    }
}

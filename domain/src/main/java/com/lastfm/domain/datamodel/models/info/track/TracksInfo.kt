package com.lastfm.domain.datamodel.models.info.track

data class TracksInfo(
     val trackInfo: List<TrackInfo>?
) {

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }

        val other = obj as TracksInfo

        val otherTrackInfoList = other.trackInfo

        if (otherTrackInfoList != null && trackInfo!!.size == otherTrackInfoList.size) {
            for (i in trackInfo.indices) {
                if (trackInfo[i] != otherTrackInfoList[i]) {
                    return false
                }
            }
        } else {
            return false
        }

        return true
    }

    override fun hashCode(): Int {
        return trackInfo?.hashCode() ?: 0
    }
}

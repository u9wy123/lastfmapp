package com.lastfm.domain.datamodel.models.search.album

data class AlbumSearchMatches(
    val albumSearches: List<AlbumSearch>
) {

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }
        val other = obj as AlbumSearchMatches

        if (other.albumSearches.size != albumSearches.size) {
            return false
        }

        for (i in albumSearches.indices) {
            val otherAlbumSearch = other.albumSearches[i]
            val albumSearch = albumSearches[i]

            if (albumSearch != otherAlbumSearch) {
                return false
            }
        }

        return true
    }

    override fun hashCode(): Int {
        return albumSearches.hashCode()
    }
}

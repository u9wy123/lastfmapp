package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.domain.datamodel.mapper.ImageMapper
import com.lastfm.data.datamodel.models.album.info.artist.ArtistInfoDto
import com.lastfm.domain.datamodel.Image
import com.lastfm.domain.datamodel.models.info.artist.ArtistInfo
import javax.inject.Inject

class ArtistInfoMapper @Inject
constructor(protected val bioInfoMapper: BioInfoMapper, protected val imageMapper: ImageMapper) {

    fun mapToDomain(data: ArtistInfoDto?): ArtistInfo {

        val imageList = data?.image
        val image = if (imageList == null) Image(
            null,
            null
        ) else imageMapper.mapToDomain(imageList[imageList.size - 1])

        return ArtistInfo(
            data?.name,
            image,
            bioInfoMapper.mapToDomain(data?.bioInfoDto)
        )
    }
}

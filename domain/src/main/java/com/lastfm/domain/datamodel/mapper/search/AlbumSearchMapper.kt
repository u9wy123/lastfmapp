package com.lastfm.domain.datamodel.mapper.search

import com.lastfm.domain.datamodel.mapper.ImageMapper
import com.lastfm.data.datamodel.models.album.search.AlbumSearchDto
import com.lastfm.domain.datamodel.Image
import com.lastfm.domain.datamodel.models.search.album.AlbumSearch
import javax.inject.Inject

class AlbumSearchMapper @Inject constructor(protected val imageMapper: ImageMapper) {

    fun mapToDomain(data: AlbumSearchDto): AlbumSearch {

        val image = if (data.image == null) Image(
            null,
            null
        ) else imageMapper.mapToDomain(data.image!![data.image!!.size - 1])

        return AlbumSearch(data.name, data.artist, image)
    }
}

package com.lastfm.domain.datamodel.models.info.track

import com.lastfm.domain.datamodel.models.info.artist.ArtistInfo


data class TrackInfo(
    val name: String? = null,
    val duration: String? = null,
    val artistInfo: ArtistInfo? = null
) {
    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }
        val other = obj as TrackInfo
        return (this.name == other.name
                && this.duration == other.duration
                && this.artistInfo == other.artistInfo)
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + (duration?.hashCode() ?: 0)
        result = 31 * result + (artistInfo?.hashCode() ?: 0)
        return result
    }
}

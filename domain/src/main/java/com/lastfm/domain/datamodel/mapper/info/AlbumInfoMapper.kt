package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.domain.datamodel.mapper.ImageMapper
import com.lastfm.data.datamodel.models.album.info.album.AlbumInfoDto
import com.lastfm.domain.datamodel.Image
import com.lastfm.domain.datamodel.models.info.album.AlbumInfo

import javax.inject.Inject

class AlbumInfoMapper @Inject constructor(
    protected val imageMapper: ImageMapper,
    protected val tracksInfoMapper: TracksInfoMapper
) {

    fun mapToDomain(data: AlbumInfoDto?): AlbumInfo {

        if(data == null)
            return AlbumInfo()

        val imageList = data.image

        return AlbumInfo(
            data.name,
            data.artist,
            if (imageList == null) Image(
                null,
                null
            ) else imageMapper.mapToDomain(imageList[imageList.size - 1]),
            tracksInfoMapper.mapToDomain(data.tracksInfoDto)
        )
    }
}

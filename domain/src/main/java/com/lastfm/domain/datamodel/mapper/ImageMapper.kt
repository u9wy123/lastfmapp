package com.lastfm.domain.datamodel.mapper

import com.lastfm.data.datamodel.models.ImageDto
import com.lastfm.domain.datamodel.Image

import javax.inject.Inject

class ImageMapper @Inject constructor(){

    fun mapToDomain(data: ImageDto): Image {
        return Image(data.text ?: "", data.size ?: "")
    }
}

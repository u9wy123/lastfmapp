package com.lastfm.domain.datamodel.models.search.artist

import com.lastfm.domain.datamodel.Image



data class ArtistSearch(
    val name: String? = null,
    val image: Image? = null
) {

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }
        val other = obj as ArtistSearch
        return this.name == other.name && this.image == other.image
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + (image?.hashCode() ?: 0)
        return result
    }
}

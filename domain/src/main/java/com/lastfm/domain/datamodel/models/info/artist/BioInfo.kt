package com.lastfm.domain.datamodel.models.info.artist

data class BioInfo(val summary: String? = null) {

    override fun equals(obj: Any?): Boolean {

        if (this === obj) {
            return true
        }

        if (obj !is BioInfo) {
            return false
        }

        return if (summary != null) summary == obj.summary else obj.summary == null
    }

    override fun hashCode(): Int {
        return summary?.hashCode() ?: 0
    }
}

package com.lastfm.domain.datamodel.models.search.album

import com.lastfm.domain.datamodel.Image



data class AlbumSearch(
    val name: String? = null,
    val artist: String? = null,
    val image: Image? = null
) {

    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }
        val other = obj as AlbumSearch
        return (this.name == other.name
                && this.artist == other.artist
                && this.image == other.image)
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + (artist?.hashCode() ?: 0)
        result = 31 * result + (image?.hashCode() ?: 0)
        return result
    }
}

package com.lastfm.domain.datamodel.models.info.album

import com.lastfm.domain.datamodel.Image
import com.lastfm.domain.datamodel.models.info.track.TracksInfo



data class AlbumInfo(
    val name: String? = null,
    val artist: String? = null,
    val image: Image? = null,
    val trackInfos: TracksInfo? = null
) {
    override fun equals(obj: Any?): Boolean {
        if (this === obj) {
            return true
        }
        if (obj == null || javaClass != obj.javaClass) {
            return false
        }
        val other = obj as AlbumInfo

        return !(this.name != other.name ||
                this.artist != other.artist ||
                this.image != other.image ||
                this.trackInfos != other.trackInfos)
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + (artist?.hashCode() ?: 0)
        result = 31 * result + (image?.hashCode() ?: 0)
        result = 31 * result + (trackInfos?.hashCode() ?: 0)
        return result
    }
}

package com.lastfm.domain.datamodel

data class Image(val text: String? = null, val size: String? = null) {

    override fun equals(obj: Any?): Boolean {

        if (obj == null || obj !is Image) {
            return false
        }

        return !(text != obj.text || size != obj.size)
    }

    override fun hashCode(): Int {
        var result = text.hashCode()
        result = 31 * result + size.hashCode()
        return result
    }
}

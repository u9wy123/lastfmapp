package com.lastfm.domain.datamodel.mapper.info

import com.lastfm.data.datamodel.models.album.info.track.TrackInfoDto
import com.lastfm.domain.datamodel.models.info.track.TrackInfo

import javax.inject.Inject

class TrackInfoMapper @Inject constructor(protected val artistInfoMapper: ArtistInfoMapper) {

    fun mapToDomain(data: TrackInfoDto?): TrackInfo {

        if(data == null)
            return TrackInfo()

        return TrackInfo(
            data.name,
            data.duration,
            artistInfoMapper.mapToDomain(data.artistInfoDto))
    }
}

package com.lastfm.domain.interactors

import com.lastfm.data.datasource.remote.SearchDataSource
import com.lastfm.domain.datamodel.mapper.search.AlbumSearchMatchesMapper
import com.lastfm.domain.datamodel.mapper.search.ArtistSearchMatchesMapper
import com.lastfm.domain.datamodel.mapper.search.TrackSearchMatchesMapper
import com.lastfm.domain.datamodel.models.search.SearchMatches
import io.reactivex.Single
import io.reactivex.functions.Function3
import javax.inject.Inject

class SearchInteractor @Inject constructor(
    protected val searchDataSource: SearchDataSource,
    protected val trackSearchMatchesMapper: TrackSearchMatchesMapper,
    protected val albumSearchMatchesMapper: AlbumSearchMatchesMapper,
    protected val artistSearchMatchesMapper: ArtistSearchMatchesMapper
) {

    fun search(search: String): Single<SearchMatches> {

        return Single.zip(searchDataSource.searchTracks(search),
            searchDataSource.searchAlbums(search),
            searchDataSource.searchArtists(search),
            Function3 { trackResponse, albumResponse, artistResponse ->
                SearchMatches(
                    trackSearchMatchesMapper.mapToDomain(trackResponse.trackSearchResultsDto?.trackMatches),
                    albumSearchMatchesMapper.mapToDomain(albumResponse.albumSearchResultsDto?.albumMatches),
                    artistSearchMatchesMapper.mapToDomain(artistResponse.artistSearchResultsDto?.artistMatches)
                )
            }
        )
    }
}


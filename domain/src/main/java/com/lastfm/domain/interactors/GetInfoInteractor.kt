package com.lastfm.domain.interactors

import com.lastfm.domain.datamodel.mapper.info.AlbumInfoMapper
import com.lastfm.domain.datamodel.mapper.info.ArtistInfoMapper
import com.lastfm.domain.datamodel.mapper.info.TrackInfoMapper
import com.lastfm.data.datasource.IGetInfoDataSource
import com.lastfm.domain.datamodel.models.info.album.AlbumInfo
import com.lastfm.domain.datamodel.models.info.artist.ArtistInfo
import com.lastfm.domain.datamodel.models.info.track.TrackInfo
import io.reactivex.Single
import javax.inject.Inject

class GetInfoInteractor @Inject constructor(
    protected val getInfoDataSource: IGetInfoDataSource,
    protected val artistInfoMapper: ArtistInfoMapper,
    protected val albumInfoMapper: AlbumInfoMapper,
    protected val trackInfoMapper: TrackInfoMapper
) {

    fun getArtistInfo(artistName: String): Single<ArtistInfo> {
        return getInfoDataSource.getArtistInfo(artistName)
            .map { artistInfoMapper.mapToDomain(it.artistInfoDto) }
    }

    fun getAlbumInfo(albumName: String, artistName: String): Single<AlbumInfo> {
        return getInfoDataSource.getAlbumInfo(albumName,artistName)
            .map { albumInfoMapper.mapToDomain(it.albumInfoDto) }
    }

    fun getTrackInfo(trackName: String, artistName: String): Single<TrackInfo> {
        return getInfoDataSource.getTrackInfo(trackName,artistName)
            .map { trackInfoMapper.mapToDomain(it.track) }
    }
}
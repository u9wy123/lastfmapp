package com.lastfm.data.datasource.remote

import com.lastfm.data.datamodel.models.album.info.album.AlbumInfoResponseDto
import com.lastfm.data.datamodel.models.album.info.artist.ArtistInfoResponseDto
import com.lastfm.data.datamodel.models.album.info.track.TrackInfoResponseDto
import com.lastfm.data.datasource.IGetInfoDataSource
import com.lastfm.data.network.endpoints.InfoApi
import io.reactivex.Single
import javax.inject.Inject

class GetInfoDataSource @Inject constructor(
    private val infoApi: InfoApi
) : IGetInfoDataSource {

    override fun getArtistInfo(artistName: String): Single<ArtistInfoResponseDto> {
        return infoApi.getArtistInfo(artistName)
    }

    override fun getAlbumInfo(albumName: String, artistName: String): Single<AlbumInfoResponseDto> {
        return infoApi.getAlbumsInfo(albumName, artistName)
    }

    override fun getTrackInfo(trackName: String, artistName: String): Single<TrackInfoResponseDto> {
        return infoApi.getTrackInfo(trackName, artistName)
    }
}

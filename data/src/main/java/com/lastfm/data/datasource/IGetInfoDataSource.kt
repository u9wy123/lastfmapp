package com.lastfm.data.datasource

import com.lastfm.data.datamodel.models.album.info.album.AlbumInfoResponseDto
import com.lastfm.data.datamodel.models.album.info.artist.ArtistInfoResponseDto
import com.lastfm.data.datamodel.models.album.info.track.TrackInfoResponseDto
import io.reactivex.Single

interface IGetInfoDataSource {

    fun getArtistInfo(artistName: String): Single<ArtistInfoResponseDto>

    fun getAlbumInfo(albumName: String, artistName: String): Single<AlbumInfoResponseDto>

    fun getTrackInfo(trackName: String, artistName: String): Single<TrackInfoResponseDto>
}
package com.lastfm.data.datasource

import com.lastfm.data.datamodel.models.album.search.AlbumSearchResponseDto
import com.lastfm.data.datamodel.models.artist.search.ArtistSearchResponseDto
import com.lastfm.data.datamodel.models.track.search.TrackSearchResponseDto
import io.reactivex.Single

interface ISearchDataSource {
    fun searchArtists(artist: String): Single<ArtistSearchResponseDto>

    fun searchAlbums(album: String): Single<AlbumSearchResponseDto>

    fun searchTracks(track: String): Single<TrackSearchResponseDto>
}
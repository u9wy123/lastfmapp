package com.lastfm.data.datasource.remote

import com.lastfm.data.datamodel.models.album.search.AlbumSearchResponseDto
import com.lastfm.data.datamodel.models.artist.search.ArtistSearchResponseDto
import com.lastfm.data.datamodel.models.track.search.TrackSearchResponseDto
import com.lastfm.data.datasource.ISearchDataSource
import com.lastfm.data.network.endpoints.SearchApi
import io.reactivex.Single
import javax.inject.Inject

class SearchDataSource @Inject constructor(
    private val searchApi: SearchApi
) : ISearchDataSource {
    override fun searchArtists(artist: String): Single<ArtistSearchResponseDto> {
        return searchApi.searchArtists(artist)
    }

    override fun searchAlbums(album: String): Single<AlbumSearchResponseDto> {
        return searchApi.searchAlbums(album)
    }

    override fun searchTracks(track: String): Single<TrackSearchResponseDto> {
        return searchApi.searchTracks(track)
    }
}

package com.lastfm.data.dependency.injection

import com.lastfm.data.datasource.remote.GetInfoDataSource
import com.lastfm.data.datasource.remote.SearchDataSource
import com.lastfm.data.network.endpoints.InfoApi
import com.lastfm.data.network.endpoints.SearchApi
import dagger.Module
import dagger.Provides

@Module(includes = [NetworkModule::class])
class DataSourceModule {

    @Provides
    fun provideSearchDataSource(searchApi: SearchApi) = SearchDataSource(searchApi)

    @Provides
    fun providesInfoDataSource(infoApi: InfoApi) = GetInfoDataSource(infoApi)

}
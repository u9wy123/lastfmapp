package com.lastfm.data.dependency.injection

import android.content.Context

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.lastfm.data.network.BaseUrls
import com.lastfm.data.network.endpoints.InfoApi
import com.lastfm.data.network.endpoints.SearchApi
import com.lastfm.data.network.interceptors.ApiKeyInterceptor
import com.lastfm.data.network.interceptors.NetworkConnectivityInterceptor
import com.lastfm.data.network.interceptors.NetworkErrorResponseInterceptor
import com.lastfm.data.network.interceptors.ResponseFormatInterceptor
import com.squareup.picasso.Picasso

import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    @Singleton
    internal fun provideGson() =
        GsonBuilder().create()

    @Provides
    @Singleton
    internal fun provideGsonConverterFactory(gson: Gson) =
        GsonConverterFactory.create(gson)


    @Provides
    @Singleton
    internal fun provideOkHttpClient(
        apiKeyInterceptor: ApiKeyInterceptor,
        responseFormatInterceptor: ResponseFormatInterceptor,
        networkErrorResponseInterceptor : NetworkErrorResponseInterceptor,
        networkConnectivityInterceptor: NetworkConnectivityInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(REQUEST_TIMEOUT_PERIOD_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(REQUEST_TIMEOUT_PERIOD_SECONDS, TimeUnit.SECONDS)
            .addInterceptor(responseFormatInterceptor)
            .addInterceptor(apiKeyInterceptor)
            .addInterceptor(networkErrorResponseInterceptor)
            .addInterceptor(networkConnectivityInterceptor)
            .addInterceptor(HttpLoggingInterceptor().apply {
                this.level = HttpLoggingInterceptor.Level.BODY
            })            .build()
    }

    @Provides
    @Singleton
    internal fun providesPicasso(context: Context) =
        Picasso.Builder(context)
            .executor(Executors.newSingleThreadExecutor())
            .build()

    @Provides
    @Singleton
    internal fun provideCallAdaptorFactory() = RxJava2CallAdapterFactory.create()

    @Provides
    @Singleton
    internal fun provideRestAdapter(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory,
        callAdapterFactory: RxJava2CallAdapterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BaseUrls.BASE_LAST_FM_URL)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(callAdapterFactory)
            .build()
    }

    @Provides
    @Singleton
    internal fun proivdeSearchApi(retrofit: Retrofit) =
        retrofit.create(SearchApi::class.java)


    @Provides
    @Singleton
    internal fun proivdeInfoApi(retrofit: Retrofit) = retrofit.create(InfoApi::class.java)

}
private const val REQUEST_TIMEOUT_PERIOD_SECONDS: Long = 10

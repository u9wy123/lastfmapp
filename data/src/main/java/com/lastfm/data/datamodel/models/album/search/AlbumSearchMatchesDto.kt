package com.lastfm.data.datamodel.models.album.search

import com.google.gson.annotations.SerializedName




data class AlbumSearchMatchesDto(@SerializedName("album") val albums: List<AlbumSearchDto>? = null)

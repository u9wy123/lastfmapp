package com.lastfm.data.datamodel.models.track.search

import com.google.gson.annotations.SerializedName




data class TrackSearchMatchesDto(
    @SerializedName("track") var tracks: List<TrackSearchDto>? = null
)


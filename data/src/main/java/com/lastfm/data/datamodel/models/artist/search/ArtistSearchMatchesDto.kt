package com.lastfm.data.datamodel.models.artist.search

import com.google.gson.annotations.SerializedName




data class ArtistSearchMatchesDto(@SerializedName("artist") val artists: List<ArtistSearchDto>? = null)

package com.lastfm.data.datamodel.models.album.info.track

import com.google.gson.annotations.SerializedName

data class TracksInfoDto(@SerializedName("track") val trackInfoDto: List<TrackInfoDto>? = null)

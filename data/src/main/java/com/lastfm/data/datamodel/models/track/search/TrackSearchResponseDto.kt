package com.lastfm.data.datamodel.models.track.search

import com.google.gson.annotations.SerializedName




data class TrackSearchResponseDto(
    @SerializedName("results") val trackSearchResultsDto: TrackSearchResultsDto? = null
)

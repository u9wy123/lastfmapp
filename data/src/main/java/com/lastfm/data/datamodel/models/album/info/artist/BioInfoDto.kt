package com.lastfm.data.datamodel.models.album.info.artist

import com.google.gson.annotations.SerializedName




data class BioInfoDto(@SerializedName("summary") val summary: String? = null)

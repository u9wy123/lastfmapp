package com.lastfm.data.datamodel.models.album.info.artist

import com.google.gson.annotations.SerializedName




data class ArtistInfoResponseDto (
    @SerializedName("artist") val artistInfoDto: ArtistInfoDto? = null
)

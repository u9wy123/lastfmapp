package com.lastfm.data.datamodel.models.album.info.album

import com.google.gson.annotations.SerializedName
import com.lastfm.data.datamodel.models.ImageDto
import com.lastfm.data.datamodel.models.album.info.track.TracksInfoDto




data class AlbumInfoDto(
    @SerializedName("name") val name: String? = null,
    @SerializedName("artist") val artist: String? = null,
    @SerializedName("mbid") val mbid: String? = null,
    @SerializedName("image") val image: List<ImageDto>? = null,
    @SerializedName("tracks") val tracksInfoDto: TracksInfoDto? = null
)

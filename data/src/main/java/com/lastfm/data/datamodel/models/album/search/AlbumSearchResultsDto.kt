package com.lastfm.data.datamodel.models.album.search

import com.google.gson.annotations.SerializedName




data class AlbumSearchResultsDto (@SerializedName("albummatches") val albumMatches: AlbumSearchMatchesDto? = null)

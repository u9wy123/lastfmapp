package com.lastfm.data.datamodel.models.track.search

import com.google.gson.annotations.SerializedName
import com.lastfm.data.datamodel.models.ImageDto




data class TrackSearchDto(
    @SerializedName("name") val name: String? = null,
    @SerializedName("artist") val artist: String? = null,
    @SerializedName("url") val url: String? = null,
    @SerializedName("streamable") val streamable: String? = null,
    @SerializedName("listeners") val listeners: String? = null,
    @SerializedName("image") val image: List<ImageDto>? = null,
    @SerializedName("mbid") val mbid: String? = null
)

package com.lastfm.data.datamodel.models.album.info.album

import com.google.gson.annotations.SerializedName

data class AlbumInfoResponseDto (@SerializedName("album") val albumInfoDto: AlbumInfoDto? = null)

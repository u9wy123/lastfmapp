package com.lastfm.data.datamodel.models.album.info.artist

import com.google.gson.annotations.SerializedName
import com.lastfm.data.datamodel.models.ImageDto



data class ArtistInfoDto(
        @SerializedName("name") val name: String? = null,
        @SerializedName("image") val image: List<ImageDto>? = null,
        @SerializedName("bio") val bioInfoDto: BioInfoDto? = null
)

package com.lastfm.data.datamodel.models

import com.google.gson.annotations.SerializedName




data class ImageDto(
    @SerializedName("#text") val text: String? = null,
    @SerializedName("size") val size: String? = null
)

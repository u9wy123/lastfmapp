package com.lastfm.data.datamodel.models.track.search

import com.google.gson.annotations.SerializedName




data class TrackSearchResultsDto(
    @SerializedName("trackmatches") val trackMatches: TrackSearchMatchesDto? = null
)

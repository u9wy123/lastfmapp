package com.lastfm.data.datamodel.models.album.info.track

import com.google.gson.annotations.SerializedName
import com.lastfm.data.datamodel.models.album.info.artist.ArtistInfoDto




data class TrackInfoDto(@SerializedName("name") val name: String? = null,
                        @SerializedName("duration") val duration: String? = null,
                        @SerializedName("artist") val artistInfoDto: ArtistInfoDto? = null
)

package com.lastfm.data.datamodel.models.album.search

import com.google.gson.annotations.SerializedName




data class AlbumSearchResponseDto(@SerializedName("results") val albumSearchResultsDto: AlbumSearchResultsDto? = null)

package com.lastfm.data.datamodel.models.album.info.track

import com.google.gson.annotations.SerializedName




data class TrackInfoResponseDto(@SerializedName("track") val track: TrackInfoDto? = null)

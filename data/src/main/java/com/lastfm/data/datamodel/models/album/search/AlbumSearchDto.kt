package com.lastfm.data.datamodel.models.album.search

import com.google.gson.annotations.SerializedName
import com.lastfm.data.datamodel.models.ImageDto




data class AlbumSearchDto(@SerializedName("name") val name: String? = null,
                     @SerializedName("artist") val artist: String? = null,
                     @SerializedName("url") val url: String? = null,
                     @SerializedName("image") val image: List<ImageDto>? = null,
                     @SerializedName("streamable") val streamable: String? = null,
                     @SerializedName("mbid") val mbid: String? = null
)

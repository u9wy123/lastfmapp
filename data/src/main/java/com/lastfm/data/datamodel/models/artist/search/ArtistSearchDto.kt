package com.lastfm.data.datamodel.models.artist.search

import com.google.gson.annotations.SerializedName
import com.lastfm.data.datamodel.models.ImageDto




data class ArtistSearchDto(@SerializedName("name") val name: String? = null,
                           @SerializedName("listeners") val listeners: String? = null,
                           @SerializedName("mbid") val mbid: String? = null,
                           @SerializedName("url") val url: String? = null,
                           @SerializedName("streamable") val streamable: String? = null,
                           @SerializedName("image") val image: List<ImageDto>? = null
)

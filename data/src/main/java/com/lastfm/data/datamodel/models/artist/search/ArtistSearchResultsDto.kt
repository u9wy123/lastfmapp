package com.lastfm.data.datamodel.models.artist.search

import com.google.gson.annotations.SerializedName




data class ArtistSearchResultsDto(@SerializedName("artistmatches") val artistMatches: ArtistSearchMatchesDto? = null)

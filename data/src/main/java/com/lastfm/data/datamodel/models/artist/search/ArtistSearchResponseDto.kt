package com.lastfm.data.datamodel.models.artist.search

import com.google.gson.annotations.SerializedName




data class ArtistSearchResponseDto(@SerializedName("results") val artistSearchResultsDto: ArtistSearchResultsDto? = null)

package com.lastfm.data.network.interceptors

import android.content.Context
import com.data.network.exceptions.NoNetworkException
import com.lastfm.data.util.NetworkUtil
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class NetworkConnectivityInterceptor @Inject constructor(private val context : Context): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        if (!NetworkUtil.isOnline(context)) {
            throw NoNetworkException()
        }

        return chain.proceed(chain.request().newBuilder().build())
    }

}
package com.lastfm.data.network.interceptors

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class ResponseFormatInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val originalRequest = chain.request()

        val requestBuilder = originalRequest.newBuilder()
        requestBuilder.url(
            originalRequest.url.toString() +
                    APPEND + RESPONSE_LIMIT_VAR + RESPONSE_LIMIT +
                    APPEND + RESPONSE_FORMAT_VAR + RESPONSE_FORMAT
        )

        return chain.proceed(requestBuilder.build())
    }
}

private const val RESPONSE_FORMAT_VAR = "format="
private const val RESPONSE_FORMAT = "json"

private const val RESPONSE_LIMIT_VAR = "limit="
private const val RESPONSE_LIMIT = "10"

private const val APPEND = "&"

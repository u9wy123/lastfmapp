package com.lastfm.data.network.interceptors

import com.data.network.exceptions.*
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class NetworkErrorResponseInterceptor @Inject constructor(): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val response = chain.proceed(chain.request().newBuilder().build())

        when (response.code) {
            in 405..499, 400 -> throw NetworkException(response.code, response.message)
            401 -> throw UnauthorisedException()
            403 -> throw ForbiddenException()
            404 -> throw NotFoundException()
            in 500..599 -> throw ServerException(response.code, response.message)
        }
        return response
    }
}
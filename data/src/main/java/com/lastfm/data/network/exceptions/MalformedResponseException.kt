package com.data.network.exceptions

import java.io.IOException

class MalformedResponseException(
    val reason: String? = null
) : IOException() {

    override val message: String?
        get() = reason ?: "Malformed Response"
}

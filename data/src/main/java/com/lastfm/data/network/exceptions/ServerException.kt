package com.data.network.exceptions

class ServerException(code: Int, message: String) : NetworkException(code, message)

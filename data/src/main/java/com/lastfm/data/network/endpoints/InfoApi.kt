package com.lastfm.data.network.endpoints

import com.lastfm.data.datamodel.models.album.info.album.AlbumInfoResponseDto
import com.lastfm.data.datamodel.models.album.info.artist.ArtistInfoResponseDto
import com.lastfm.data.datamodel.models.album.info.track.TrackInfoResponseDto

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface InfoApi {

    @GET("?method=artist.getInfo")
    fun getArtistInfo(@Query("artist") artistName: String): Single<ArtistInfoResponseDto>

    @GET("?method=album.getInfo")
    fun getAlbumsInfo(@Query("album") albumName: String,
        @Query("artist") artistName: String
    ): Single<AlbumInfoResponseDto>

    @GET("?method=track.getInfo")
    fun getTrackInfo(
        @Query("track") trackName: String,
        @Query("artist") artistName: String
    ): Single<TrackInfoResponseDto>

}

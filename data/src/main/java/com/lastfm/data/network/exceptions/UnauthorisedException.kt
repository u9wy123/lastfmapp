package com.data.network.exceptions

import java.io.IOException

class UnauthorisedException : IOException() {

    override val message: String?
        get() = "Unauthorised Access"
}

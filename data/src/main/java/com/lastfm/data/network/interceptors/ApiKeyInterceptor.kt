package com.lastfm.data.network.interceptors

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class ApiKeyInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val originalRequest = chain.request()

        val requestBuilder = originalRequest.newBuilder()
        requestBuilder.url(originalRequest.url.toString() + APPEND + API_PARAM_VAR + API_KEY)

        return chain.proceed(requestBuilder.build())
    }
}

private const val API_PARAM_VAR = "api_key="
private const val API_KEY = "e324cfd83b986007366022aab72d3483"
private const val APPEND = "&"

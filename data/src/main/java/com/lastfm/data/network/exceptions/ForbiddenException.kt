package com.data.network.exceptions

import java.io.IOException

class ForbiddenException : IOException() {

    override val message: String?
        get() = "Forbidden Access"
}

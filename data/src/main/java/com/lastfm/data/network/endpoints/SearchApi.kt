package com.lastfm.data.network.endpoints

import com.lastfm.data.datamodel.models.album.search.AlbumSearchResponseDto
import com.lastfm.data.datamodel.models.artist.search.ArtistSearchResponseDto
import com.lastfm.data.datamodel.models.track.search.TrackSearchResponseDto

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("?method=artist.search")
    fun searchArtists(@Query("artist") artistName: String): Single<ArtistSearchResponseDto>

    @GET("?method=album.search")
    fun searchAlbums(@Query("album") albumName: String): Single<AlbumSearchResponseDto>

    @GET("?method=track.search")
    fun searchTracks(@Query("track") trackName: String): Single<TrackSearchResponseDto>
}

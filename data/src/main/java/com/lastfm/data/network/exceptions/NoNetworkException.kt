package com.data.network.exceptions

import java.io.IOException

class NoNetworkException : IOException() {

    override val message: String?
        get() = "No Network Connectivity"
}

package com.lastfm.presentation.modules.presenter

import com.lastfm.domain.datamodel.Image
import com.lastfm.domain.datamodel.models.info.album.AlbumInfo
import com.lastfm.domain.datamodel.models.info.artist.ArtistInfo
import com.lastfm.domain.datamodel.models.info.track.TrackInfo
import com.lastfm.domain.datamodel.models.search.SearchMatches
import com.lastfm.domain.datamodel.models.search.album.AlbumSearch
import com.lastfm.domain.datamodel.models.search.album.AlbumSearchMatches
import com.lastfm.domain.datamodel.models.search.artist.ArtistSearch
import com.lastfm.domain.datamodel.models.search.artist.ArtistSearchMatches
import com.lastfm.domain.datamodel.models.search.track.TrackSearch
import com.lastfm.domain.datamodel.models.search.track.TrackSearchMatches
import com.lastfm.domain.interactors.GetInfoInteractor
import com.lastfm.domain.interactors.SearchInteractor
import com.lastfm.presentation.SchedulerProvider
import com.lastfm.presentation.ui.modules.search.ISearch
import com.lastfm.presentation.ui.modules.search.presenter.SearchPresenter

import org.junit.Before
import org.junit.Test
import org.mockito.Mock

import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers

import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.ArgumentMatchers.anyList
import org.mockito.ArgumentMatchers.anyString
import org.mockito.InjectMocks
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class SearchPresenterTest {

    @Mock
    private lateinit var view: ISearch.View

    @Mock
    private lateinit var searchInteractor: SearchInteractor

    @Mock
    private lateinit var getInfoInteractor: GetInfoInteractor

    @InjectMocks
    private lateinit var presenter: SearchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        SchedulerProvider.io = Schedulers.trampoline()
        SchedulerProvider.main = Schedulers.trampoline()

        presenter.setView(view)
        presenter.onViewReady()

        `when`(searchInteractor.search(SEARCH))
            .thenReturn(Single.just(createSearchMatches()))

        `when`(getInfoInteractor.getTrackInfo(anyString(), anyString()))
            .thenReturn(Single.just(createTrackInfo()))

        `when`(getInfoInteractor.getAlbumInfo(anyString(), anyString()))
            .thenReturn(Single.just(createAlbumInfo()))

        `when`(getInfoInteractor.getArtistInfo(anyString()))
            .thenReturn(Single.just(createArtistInfo()))
    }

    @Test
    fun onSearchCallsSearchInteractorSearch() {
        presenter.onSearch(SEARCH)
        verify(searchInteractor).search(anyString())
    }

    @Test
    fun onSearchCallsViewShowSearchResults() {
        presenter.onSearch(SEARCH)
        verify(view).showSearchResults(anyList())
    }

    @Test
    fun onSearchCallShowsAndHidesLoading() {
        presenter.onSearch(SEARCH)
        verify(view, times(2)).showLoading(anyBoolean())
    }

    @Test
    fun onGetTrackInfoCallsGetInfoInteractor() {
        presenter.onSearch(SEARCH)
        presenter.onItemSelected(TRACK_POSITION)
        verify(getInfoInteractor).getTrackInfo(anyString(), anyString())
    }

    @Test
    fun onGetTrackInfoShowsInfoDialog() {
        presenter.onSearch(SEARCH)
        presenter.onItemSelected(TRACK_POSITION)
        verify(view).showTrackDialog(createTrackInfo())
    }

    @Test
    fun onGetTrackInfoShowsAndHidesLoading() {
        presenter.onSearch(SEARCH)
        presenter.onItemSelected(TRACK_POSITION)
        verify(view, times(4)).showLoading(anyBoolean())
    }

    @Test
    fun onGetAlbumInfoCallsGetInfoInteractor() {
        presenter.onSearch(SEARCH)
        presenter.onItemSelected(ALBUM_POSITION)
        verify(getInfoInteractor).getAlbumInfo(anyString(), anyString())
    }

    @Test
    fun onGetAlbumInfoShowsInfoDialog() {
        presenter.onSearch(SEARCH)
        presenter.onItemSelected(ALBUM_POSITION)
        verify(view).showAlbumDialog(createAlbumInfo())
    }

    @Test
    fun onGetAlbumInfoShowsAndHidesLoading() {
        presenter.onSearch(SEARCH)
        presenter.onItemSelected(ALBUM_POSITION)
        verify(view, times(4)).showLoading(anyBoolean())
    }

    @Test
    fun onGetArtistInfoCallsGetInfoInteractor() {
        presenter.onSearch(SEARCH)
        presenter.onItemSelected(ARTIST_POSITION)
        verify(getInfoInteractor).getArtistInfo(anyString())
    }

    @Test
    fun onGetArtistInfoShowsInfoDialog() {
        presenter.onSearch(SEARCH)
        presenter.onItemSelected(ARTIST_POSITION)
        verify(view).showArtistDialog(createArtistInfo())
    }

    @Test
    fun onGetArtistInfoShowsAndHidesLoading() {
        presenter.onSearch(SEARCH)
        presenter.onItemSelected(ARTIST_POSITION)
        verify(view, times(4)).showLoading(anyBoolean())
    }

    private fun createSearchMatches(): SearchMatches {

        return SearchMatches(
            TrackSearchMatches(listOf(TrackSearch(TRACK, ARTIST, Image("text", "size")))),
            AlbumSearchMatches(listOf(AlbumSearch(ALBUM, ARTIST))),
            ArtistSearchMatches(listOf(ArtistSearch(ARTIST)))
        )
    }

    private fun createTrackInfo(): TrackInfo {
        return TrackInfo()
    }

    private fun createAlbumInfo(): AlbumInfo {
        return AlbumInfo()
    }

    private fun createArtistInfo(): ArtistInfo {
        return ArtistInfo()
    }
}

private const val SEARCH = "search"
private const val TRACK = "track"
private const val ALBUM = "album"
private const val ARTIST = "artist"
private const val TRACK_POSITION = 1
private const val ALBUM_POSITION = 3
private const val ARTIST_POSITION = 5

package com.lastfm.presentation

import android.app.Application
import android.content.Context

import com.lastfm.data.dependency.injection.NetworkModule
import com.lastfm.presentation.dependency.injection.DaggerLastFmAppComponent
import com.lastfm.presentation.dependency.injection.LastFmAppComponent
import com.lastfm.presentation.dependency.injection.LastFmAppModule

class LastFmApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = this
        initialiseInjector()
    }

    private fun initialiseInjector() {
        appComponent = DaggerLastFmAppComponent.builder()
            .lastFmAppModule(LastFmAppModule(this))
            .networkModule(NetworkModule())
            .build()
    }

    companion object {
        var appContext: Context? = null
            private set
        var appComponent: LastFmAppComponent? = null
            private set
    }
}

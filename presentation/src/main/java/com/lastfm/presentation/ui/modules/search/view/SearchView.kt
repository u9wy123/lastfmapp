package com.lastfm.presentation.ui.modules.search.view

import android.app.AlertDialog
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast

import com.lastfm.domain.datamodel.models.info.album.AlbumInfo
import com.lastfm.domain.datamodel.models.info.artist.ArtistInfo
import com.lastfm.domain.datamodel.models.info.track.TrackInfo
import com.lastfm.domain.datamodel.models.info.track.TracksInfo
import com.lastfm.presentation.R
import com.lastfm.presentation.ui.modules.search.ISearch

import butterknife.BindView
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.view_search.view.*

class SearchView @JvmOverloads constructor(
    private val presenter: ISearch.Presenter,
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr), ISearch.View {

    private var searchAdapter: SearchAdapter? = null

    private val onItemClickListener = object : SearchAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                presenter.onItemSelected(position)
            }
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.view_search, this)

        button_search?.setOnClickListener { view -> presenter.onSearch(editText_search.text.toString()) }

        presenter.setView(this)
        presenter.onViewReady()
    }

    override fun showSearchResults(searchResults: List<Any>) {
        if (searchAdapter == null)
            setUpAdapter(searchResults)
        else
            searchAdapter?.notifyDataSetChanged()
    }

    override fun showError(message: String?) {
        Toast.makeText(context, message ?: context.getString(R.string.error), Toast.LENGTH_SHORT)
            .show()
    }

    override fun showLoading(isLoading: Boolean) {
        progress_indicator.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun showTrackDialog(trackInfo: TrackInfo) {
        showAlertDialog(
            R.string.track_info,
            context.getString(R.string.track_name, trackInfo.name) + "\n\n" +
                    context.getString(R.string.track_duration, trackInfo.duration) + "\n\n" +
                    context.getString(R.string.artist_name, trackInfo.artistInfo?.name ?: "")
        )
    }

    override fun showArtistDialog(artistInfo: ArtistInfo) {
        showAlertDialog(
            R.string.artist_info,
            context.getString(R.string.artist_name, artistInfo.name) + "\n\n" +
                    context.getString(R.string.bio, artistInfo.bioInfo!!.summary) + "\n\n"
        )
    }

    override fun showAlbumDialog(albumInfo: AlbumInfo) {
        showAlertDialog(
            R.string.album_info,
            context.getString(R.string.album_name, albumInfo.name) + "\n\n" +
                    context.getString(R.string.artist_name, albumInfo.artist) + "\n\n" +
                    context.getString(R.string.tracks,
                        albumInfo.trackInfos?.let { getTracksString(it) })
        )
    }

    private fun showAlertDialog(titleId: Int, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(titleId)
        builder.setMessage(message)
        builder.setNegativeButton(R.string.close, null)
        builder.setCancelable(false)
        builder.create().show()
    }

    private fun getTracksString(tracksInfo: TracksInfo): String {

        var tracks = ""

        for (trackInfo in tracksInfo.trackInfo!!) {
            tracks = tracks + "\n\n" + trackInfo.name + "\t" + trackInfo.duration
        }

        return tracks
    }

    private fun setUpAdapter(searchResults: List<Any>) {
        searchAdapter = SearchAdapter(searchResults)
        searchAdapter?.setItemClickListener(onItemClickListener)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = searchAdapter
    }

}

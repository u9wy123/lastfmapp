package com.lastfm.presentation.ui.modules.search.di

import com.lastfm.data.datasource.remote.GetInfoDataSource
import com.lastfm.data.datasource.remote.SearchDataSource
import com.lastfm.domain.datamodel.mapper.info.AlbumInfoMapper
import com.lastfm.domain.datamodel.mapper.info.ArtistInfoMapper
import com.lastfm.domain.datamodel.mapper.info.TrackInfoMapper
import com.lastfm.domain.datamodel.mapper.search.AlbumSearchMatchesMapper
import com.lastfm.domain.datamodel.mapper.search.ArtistSearchMatchesMapper
import com.lastfm.domain.datamodel.mapper.search.TrackSearchMatchesMapper
import com.lastfm.domain.interactors.GetInfoInteractor
import com.lastfm.domain.interactors.SearchInteractor
import com.lastfm.presentation.dependency.injection.ActivityScope
import com.lastfm.presentation.dependency.injection.LastFmAppComponent
import com.lastfm.presentation.ui.modules.search.view.SearchActivity
import com.lastfm.presentation.ui.modules.search.view.SearchAdapter
import dagger.Component
import dagger.Module
import dagger.Provides

@ActivityScope
@Component(dependencies = [LastFmAppComponent::class], modules = [SearchComponent.SearchModule::class])
interface SearchComponent {

    fun inject(adapter: SearchAdapter)

    fun inject(activity: SearchActivity)

    @Module
    class SearchModule {

        @Provides
        internal fun provideGetInfoInteractor(
            getInfoDataSource: GetInfoDataSource,
            artistInfoMapper: ArtistInfoMapper,
            albumInfoMapper: AlbumInfoMapper,
            trackInfoMapper: TrackInfoMapper
        ): GetInfoInteractor {
            return GetInfoInteractor(getInfoDataSource, artistInfoMapper, albumInfoMapper, trackInfoMapper)
        }

        @Provides
        internal fun provideSearchInteractor(
            searchDataSource: SearchDataSource,
            trackSearchMatchesMapper: TrackSearchMatchesMapper,
            albumSearchMatchesMapper: AlbumSearchMatchesMapper,
            artistSearchMatchesMapper: ArtistSearchMatchesMapper
        ): SearchInteractor {
            return SearchInteractor(searchDataSource, trackSearchMatchesMapper, albumSearchMatchesMapper, artistSearchMatchesMapper
            )
        }
    }
}

package com.lastfm.presentation.ui.modules.search.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lastfm.domain.datamodel.models.search.album.AlbumSearch
import com.lastfm.domain.datamodel.models.search.artist.ArtistSearch
import com.lastfm.domain.datamodel.models.search.track.TrackSearch
import com.lastfm.presentation.LastFmApplication
import com.lastfm.presentation.R
import com.lastfm.presentation.ui.modules.search.di.DaggerSearchComponent
import com.lastfm.presentation.ui.modules.search.model.Header
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_holder_header.view.*
import kotlinx.android.synthetic.main.view_holder_item.view.*
import javax.inject.Inject

class SearchAdapter(private val itemList: List<Any>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var itemClickListener: OnItemClickListener? = null

    @set:Inject
    lateinit var picasso: Picasso

    init {
        val eventsComponent = DaggerSearchComponent.builder()
            .lastFmAppComponent(LastFmApplication.appComponent)
            .build()
        eventsComponent.inject(this)
    }

    private inner class ItemViewHolder constructor(override val containerView: View?) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer, View.OnClickListener {

        init {
            containerView?.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            if (itemClickListener != null) {
                itemClickListener?.onItemClick(adapterPosition)
            }
        }
    }

    private inner class HeaderViewHolder constructor(override val containerView: View?) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_HEADER) {
            return HeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.view_holder_header,
                    parent,
                    false
                )
            )
        } else if (viewType == VIEW_TYPE_ITEM) {
            return ItemViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_holder_item, parent, false)
            )
        }

        throw RuntimeException("Adapter " + viewType + "not found")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        if (holder.itemViewType == VIEW_TYPE_HEADER) {
            val viewHolder = holder as HeaderViewHolder
            val (headerName) = itemList[position] as Header

            viewHolder.itemView.textView_header.text = headerName
        } else {
            val viewHolder = holder as ItemViewHolder
            val itemView = viewHolder.itemView
            val item = itemList[position]

            var imageUrl: String? = null

            when (item) {
                is TrackSearch -> {
                    val (name, artist, image) = item
                    itemView.textView_title.text = name
                    itemView.textView_subtitle.text = artist
                    imageUrl = image.text
                }
                is AlbumSearch -> {
                    val (name, artist, image) = item
                    itemView.textView_title.text = name
                    itemView.textView_subtitle.text = artist
                    imageUrl = image?.text
                }
                is ArtistSearch -> {
                    val (name, image) = item
                    itemView.textView_title.text = name
                    imageUrl = image?.text
                }
            }

            if (!imageUrl.isNullOrEmpty())
                picasso.load(imageUrl)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(itemView.imageView)

        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (itemList[position] is Header)
            VIEW_TYPE_HEADER
        else
            VIEW_TYPE_ITEM
    }

    fun setItemClickListener(itemClickListener: OnItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}

private const val VIEW_TYPE_HEADER = 0
private const val VIEW_TYPE_ITEM = 1

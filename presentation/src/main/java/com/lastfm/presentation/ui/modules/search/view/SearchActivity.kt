package com.lastfm.presentation.ui.modules.search.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.lastfm.presentation.LastFmApplication
import com.lastfm.presentation.ui.modules.search.di.DaggerSearchComponent
import com.lastfm.presentation.ui.modules.search.di.SearchComponent
import com.lastfm.presentation.ui.modules.search.presenter.SearchPresenter

import javax.inject.Inject

class SearchActivity : AppCompatActivity() {

    @Inject
    internal lateinit var searchPresenter: SearchPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val searchComponent = DaggerSearchComponent.builder()
            .lastFmAppComponent(LastFmApplication.appComponent)
            .build()

        searchComponent.inject(this)

        setContentView(SearchView(searchPresenter,this))
    }

    override fun onDestroy() {
        searchPresenter.destroy()
        super.onDestroy()
    }
}

package com.lastfm.presentation.ui.modules.search

import com.lastfm.domain.datamodel.models.info.album.AlbumInfo
import com.lastfm.domain.datamodel.models.info.artist.ArtistInfo
import com.lastfm.domain.datamodel.models.info.track.TrackInfo

interface ISearch {

    interface Presenter {

        fun setView(view: View)

        fun onViewReady()

        fun onItemSelected(position: Int)

        fun onSearch(search: String)

    }

    interface View {

        fun showSearchResults(searchResults: List<Any>)

        fun showError(message : String?)

        fun showLoading(isLoading: Boolean)

        fun showTrackDialog(trackInfo: TrackInfo)

        fun showArtistDialog(artistInfo: ArtistInfo)

        fun showAlbumDialog(albumInfo: AlbumInfo)
    }

}

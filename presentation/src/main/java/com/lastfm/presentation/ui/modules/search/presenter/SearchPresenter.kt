package com.lastfm.presentation.ui.modules.search.presenter

import com.lastfm.domain.datamodel.models.search.SearchMatches
import com.lastfm.domain.datamodel.models.search.album.AlbumSearch
import com.lastfm.domain.datamodel.models.search.artist.ArtistSearch
import com.lastfm.domain.datamodel.models.search.track.TrackSearch
import com.lastfm.domain.interactors.GetInfoInteractor
import com.lastfm.domain.interactors.SearchInteractor
import com.lastfm.presentation.SchedulerProvider
import com.lastfm.presentation.ui.modules.search.ISearch
import com.lastfm.presentation.ui.modules.search.model.Header

import java.util.ArrayList

import javax.inject.Inject

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SearchPresenter @Inject constructor(
    protected val searchInteractor: SearchInteractor,
    protected val getInfoInteractor: GetInfoInteractor
) : ISearch.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private lateinit var view: ISearch.View
    private var itemList = mutableListOf<Any>()

    override fun onViewReady() {
        // no operation
    }

    override fun setView(view: ISearch.View) {
        this.view = view
    }

    override fun onItemSelected(position: Int) {

        view.showLoading(true)

        when {
            itemList[position] is TrackSearch -> {
                val (name, artist) = itemList[position] as TrackSearch
                loadTrackInfo(name ?: "", artist ?: "")
            }
            itemList[position] is AlbumSearch -> {
                val (name, artist) = itemList[position] as AlbumSearch
                loadAlbumInfo(name ?: "", artist ?: "")
            }
            itemList[position] is ArtistSearch -> {
                val (name) = itemList[position] as ArtistSearch
                name?.let { loadArtistInfo(it) }
            }
        }

    }

    override fun onSearch(search: String) {

        view.showLoading(true)

        compositeDisposable.add(
            searchInteractor.search(search)
                .subscribeOn(SchedulerProvider.io)
                .observeOn(SchedulerProvider.main)
                .doFinally { view.showLoading(false) }
                .subscribe({ searchMatches -> buildList(searchMatches) },
                    { throwable -> view.showError(throwable.message) })
        )

    }

    private fun loadTrackInfo(trackName: String, artistName: String) {
        compositeDisposable.add(
            getInfoInteractor.getTrackInfo(trackName, artistName)
                .subscribeOn(SchedulerProvider.io)
                .observeOn(SchedulerProvider.main)
                .doFinally { view.showLoading(false) }
                .subscribe({ trackInfo -> view.showTrackDialog(trackInfo) },
                    { throwable -> view.showError(throwable.message) })
        )
    }

    private fun loadAlbumInfo(albumName: String, artistName: String) {
        compositeDisposable.add(
            getInfoInteractor.getAlbumInfo(albumName, artistName)
                .subscribeOn(SchedulerProvider.io)
                .observeOn(SchedulerProvider.main)
                .doFinally { view.showLoading(false) }
                .subscribe({ albumInfo -> view.showAlbumDialog(albumInfo) },
                    { throwable -> view.showError(throwable.message) })
        )
    }

    private fun loadArtistInfo(artistName: String) {
        compositeDisposable.add(
            getInfoInteractor.getArtistInfo(artistName)
                .subscribeOn(SchedulerProvider.io)
                .observeOn(SchedulerProvider.main)
                .doFinally { view.showLoading(false) }
                .subscribe({ artistInfo -> view.showArtistDialog(artistInfo) },
                    { throwable -> view.showError(throwable.message) })
        )
    }

    fun destroy() {
        compositeDisposable.dispose()
    }

    private fun buildList(searchMatches: SearchMatches) { //todo get strings

        itemList.clear()
        itemList.add(Header("Tracks"))
        itemList.addAll(searchMatches.trackSearchMatches.trackSearches)
        itemList.add(Header("Albums"))
        itemList.addAll(searchMatches.albumSearchMatches.albumSearches)
        itemList.add(Header("Artists"))
        itemList.addAll(searchMatches.artistSearchMatches.artistSearches)
        view.showSearchResults(itemList)
    }
}

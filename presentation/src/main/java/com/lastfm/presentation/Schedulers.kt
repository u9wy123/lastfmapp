package com.lastfm.presentation

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulerProvider {
    companion object {
        var main = AndroidSchedulers.mainThread()
        var io: Scheduler = Schedulers.io()
    }
}


package com.lastfm.presentation.dependency.injection

import android.content.Context

import com.lastfm.data.dependency.injection.NetworkModule
import com.lastfm.data.network.endpoints.InfoApi
import com.lastfm.data.network.endpoints.SearchApi
import com.squareup.picasso.Picasso

import javax.inject.Singleton

import dagger.Component

@Singleton
@Component(modules = [LastFmAppModule::class, NetworkModule::class])
interface LastFmAppComponent {

    val picasso: Picasso

    val searchApi: SearchApi

    val infoApi: InfoApi

    val context: Context

}

package com.lastfm.presentation.dependency.injection

import android.content.Context

import com.lastfm.data.dependency.injection.NetworkModule
import com.lastfm.presentation.LastFmApplication

import dagger.Module
import dagger.Provides

@Module(includes = [NetworkModule::class])
class LastFmAppModule(private val mApplication: LastFmApplication) {

    @Provides
    internal fun provideContext(): Context {
        return mApplication
    }

}
